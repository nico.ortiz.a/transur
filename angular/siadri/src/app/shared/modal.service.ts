import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';


//esta clase es la encargada de contener todas las variables observables que van
//cambiando en tiempo real
@Injectable()
export class ModalService {

  //variable que contiene el mensaje que se muestra en el Modal-Popup
  private messageSource = new BehaviorSubject<any>('default message');
  currentMessage = this.messageSource.asObservable();
  constructor() { }
  changeMessage(message: any) {
    this.messageSource.next(message);
  }

  //variable que contiene las consultas de los convenios
  private object = new BehaviorSubject<any>([]);
  currentObject = this.object.asObservable();
  changeObject(object:any){
    this.object.next(object);
  }


  private User = new BehaviorSubject<any>([]);
  currenUser = this.User.asObservable();
  changeUser(user:any){
    this.User.next(user);
  }


  
  private rutas = new BehaviorSubject<any>([]);
  currenRutas = this.rutas.asObservable();
  changeRuta(ruta:any){
    this.rutas.next(ruta);
  }


}
