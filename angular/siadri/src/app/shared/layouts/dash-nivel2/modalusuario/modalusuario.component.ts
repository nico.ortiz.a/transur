import { ModalService } from './../../../modal.service';
import { Component, OnInit } from '@angular/core';
import { FunuserService } from 'app/shared/layouts/dash-nivel2/funuser.service';

@Component({
  selector: 'app-modalusuario',
  templateUrl: './modalusuario.component.html',
  styleUrls: ['./modalusuario.component.css']
})
export class ModalusuarioComponent implements OnInit {

  user:any;
  constructor(private data: ModalService,
              private funcionUs: FunuserService) { 
    this.data.currenUser.subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() {
  }

  editaUser(){
    this.funcionUs.editarUser(this.user, this.user.$key);
  }

  eliminaUser(){
    this.funcionUs.eliminarUser(this.user.$key);
    this.funcionUs.suscribirUser().then(() => {
      this.data.changeRuta(this.funcionUs.getUser());
    });
  }

  CreaUser(){
    this.funcionUs.creaUser(this.user);
    this.funcionUs.suscribirUser().then(() => {
      this.data.changeRuta(this.funcionUs.getUser());
    });
  }

  limpiar(){
    this.user = {ruta:"",hora:"",conductor:"",cedula:"",licencia:"",placa:""};
    this.data.changeUser(this.user);
  }
}
