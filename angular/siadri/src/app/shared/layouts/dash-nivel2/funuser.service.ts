import { ModalService } from './../../modal.service';
import { Router } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';

@Injectable()
export class FunuserService {

  usuarios:any;
  coordenadas:any;
  
    constructor(private ad:AngularFireDatabase, private ruta:Router,private data:ModalService) {

    }
  
    suscribirUser() {
      // tslint:disable-next-line:prefer-const
      let prom = new Promise((resolve, reject) => {
  
        this.usuarios = null;
        this.getUsersBD().subscribe(
          user => {
                this.usuarios = user;
                resolve();
          });
      });
      return prom;
     }

  
    getUsersBD() {
     return this.ad.list('/rutas');
    }
    getUser() {
      return this.usuarios;
    }
    editarUser(user: any, key: string) {
      this.ad.app.database().ref('/rutas/' + key).set(user).then(()=>{
        alert('ruta modificada');
      });
    }
    creaUser(user: any) {
      this.ad.app.database().ref('/rutas').push(user).then(()=>{
        alert('ruta creada'); 
      });
    }

    eliminarUser(user:any){
      this.ad.app.database().ref('/rutas/'+user).remove().then(()=>{
        alert('ruta eliminada');  
      });
    }

    getcoordenadas(){
      return this.ad.list('/coordenadas');
    }




}
