import { FunuserService } from './funuser.service';
import { Subject } from 'rxjs/Rx';
import { ModalService } from './../../modal.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {HttpService} from './../../services/http.service';
import { Observable } from 'rxjs/Observable';

declare var $:any;


@Component({
  selector: 'app-dash-nivel2',
  templateUrl: './dash-nivel2.component.html',
  styleUrls: ['./dash-nivel2.component.css']
})
export class DashNivel2Component implements OnInit {

  dtTrigger = new Subject();
  
    user: any;
    key: any;

    rutas:any;

  constructor(private data:ModalService, private ad:AngularFireDatabase, private funcion:FunuserService) {
    
  
  }

ngOnInit() {
  this.funcion.suscribirUser().then(() => {
    this.data.changeRuta( this.funcion.getUser());
    this.dtTrigger.next();
  });

  this.data.currenRutas.subscribe(ruta => {
    this.rutas = ruta;
  });

}

viewUser(item: any) {
this.data.changeUser(item);
}

}


