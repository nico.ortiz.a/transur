import { FunuserService } from './../dash-nivel2/funuser.service';
import { ModalService } from './../../modal.service';
import { DashboardMapComponent } from './dashboard-map.component';
import { AngularFireDatabase } from 'angularfire2/database';
import { element } from 'protractor';
import { Injectable, OnInit } from '@angular/core';


declare var L: any;
declare var $:any;

@Injectable()
export class LeaftletmapService {
  customDefault: any;
  map:any;
 
  
   
  constructor(private func:FunuserService) {
  
  }

  plotActivity(){
    
        var myStyle = {
          "color": "#3949AB",
          "weight": 5,
          "opacity": 0.95
        };
    
        this.map = L.map('map', { center: [3.371109, -76.536738], zoom: 13, zoomControl: false });
    
    
        L.control.zoom({
            position: 'bottomleft'
        }).addTo(this.map);
    
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
          maxZoom: 18,
          id: 'mapbox.dark'
        }).addTo(this.map);
    
        var customLayer = L.geoJson(null, {
          style: myStyle
        });

      var coordenadas:any;
       this.func.getcoordenadas().subscribe(data => {
         coordenadas = data;
         console.log(coordenadas);
         
         this.pintasVariosMarker(coordenadas);
       });
       
      
    
  }

  
  layer:any = null;
  groupLayers:any=null;

  OnInit(){

  }


  pintasVariosMarker(edif){
    let DefaultIcon = L.icon({
      iconUrl: 'assets/images/marker-icon.png',
      shadowUrl: 'assets/images/marker-shadow.png'
    });

    this.groupLayers = L.layerGroup();
    for (var index = 0; index < edif.length; index++) {
      var layer = L.marker([edif[index].long,edif[index].alt],{ icon: DefaultIcon }).bindPopup(edif[index].infor);
     this.groupLayers.addLayer(layer);
    }
    this.groupLayers.addTo(this.map);

  }

  
  static trucarSides(){
    $('#content-box').addClass("collapsed-box");
    $('#div1').hide();
    $('#buton1 i').attr('class','fa fa-plus');
    $('#content-box2').removeClass("collapsed-box");
    $('#div2').show();
    $('#buton2 i').attr('class','fa fa-minus');

  }



}
