import { Observable } from 'rxjs/Observable';
import { DashboardMapComponent } from './../dashboard-map/dashboard-map.component';
import { BuscadorService } from './buscador.service';
import { ModalService } from './../../modal.service';
import { Subject } from 'rxjs/Rx';
import { any } from 'codelyzer/util/function';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
import { Promise } from 'q';

declare var EasyAutocomplete: any;
declare var $: any;

@Component({
  selector: 'app-modal-popup',
  templateUrl: './modal-popup.component.html',
  styleUrls: ['./modal-popup.component.css']
})
export class ModalPopupComponent implements OnInit {

  message: any;
  options = {
    data: [],
    getValue: "",
    list: {
      onClickEvent: function () { },
      match: { enabled: true }
    }
  };
  static easybusqueda: any;
  static buscadorService: any;
  static rutastatic: any;
  static mapstatic: any;
  static modalstatic: any;


  user = JSON.parse(localStorage.getItem('usuario'));


  constructor(private data: ModalService, private busqueda: BuscadorService) {
    ModalPopupComponent.modalstatic = data;
    this.data.currentMessage.subscribe(message => {
      console.log(message);

      this.message = message;
      this.busquedas(this.message);
    });
  }

  ngOnInit() {
  }


  busquedas(message) {
    ModalPopupComponent.rutastatic = this.busqueda.getRuta();

    if (message == 'destino') {

      let noduplicadosrutas= Array.from(new Set(ModalPopupComponent.rutastatic));
      noduplicadosrutas = this.removeDuplicates(ModalPopupComponent.rutastatic, "ruta");

      this.options.data = noduplicadosrutas;
      this.options.getValue = 'ruta';
      this.options.list.onClickEvent = function(){ModalPopupComponent.busquedaPorDestino();};

      ModalPopupComponent.easybusqueda = new EasyAutocomplete.main($('#inputBusqueda'), this.options);
      ModalPopupComponent.easybusqueda.init();

    } else if (message == 'conductor') {
      let noduplicadosrutas= Array.from(new Set(ModalPopupComponent.rutastatic));
      noduplicadosrutas = this.removeDuplicates(ModalPopupComponent.rutastatic, "conductor");

      this.options.data = noduplicadosrutas;
      this.options.getValue = 'conductor';
      this.options.list.onClickEvent = function(){ModalPopupComponent.busquedaPorConductor();};

      ModalPopupComponent.easybusqueda = new EasyAutocomplete.main($('#inputBusqueda'), this.options);
      ModalPopupComponent.easybusqueda.init();

    } else if (message == 'hora') {
      let noduplicadosrutas= Array.from(new Set(ModalPopupComponent.rutastatic));
      noduplicadosrutas = this.removeDuplicates(ModalPopupComponent.rutastatic, "hora");

      this.options.data = noduplicadosrutas;
      this.options.getValue = 'hora';
      this.options.list.onClickEvent = function(){ModalPopupComponent.busquedaPorHora();};

      ModalPopupComponent.easybusqueda = new EasyAutocomplete.main($('#inputBusqueda'), this.options);
      ModalPopupComponent.easybusqueda.init();


    }

  }


  removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject = {};

    for (var i in originalArray) {
      lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for (i in lookupObject) {
      newArray.push(lookupObject[i]);
    }
    return newArray;
  }

  static busquedaPorDestino() {
    var resultado = [];
    var item = ModalPopupComponent.easybusqueda.getSelectedItemData();

    for (var index = 0; index < ModalPopupComponent.rutastatic.length; index++) {
      var element = ModalPopupComponent.rutastatic[index];
      if (element.ruta == item.ruta) {
        resultado.push(element);
      }
    }
    console.log(resultado);
    ModalPopupComponent.modalstatic.changeObject(resultado);
    ModalPopupComponent.cerrarModal();
  }


  static busquedaPorConductor() {
    var resultado = [];
    var item = ModalPopupComponent.easybusqueda.getSelectedItemData();
    for (var index = 0; index < ModalPopupComponent.rutastatic.length; index++) {
      var element = ModalPopupComponent.rutastatic[index];
      if (element.conductor == item.conductor) {
        resultado.push(element);
      }
    }
    ModalPopupComponent.modalstatic.changeObject(resultado);
    ModalPopupComponent.cerrarModal();
  }

  static busquedaPorHora() {
    var resultado = [];
    var item = ModalPopupComponent.easybusqueda.getSelectedItemData();
    for (var index = 0; index < ModalPopupComponent.rutastatic.length; index++) {
      var element = ModalPopupComponent.rutastatic[index];
      if (element.hora == item.hora) {
        resultado.push(element);
      }
    }
    ModalPopupComponent.modalstatic.changeObject(resultado);
    ModalPopupComponent.cerrarModal();
  }



  static cerrarModal() {
    $('#modal1').modal('hide');
    $('#content-box').addClass("collapsed-box");
    $('#div1').hide();
    $('#buton1 i').attr('class', 'fa fa-plus');
    $('#content-box2').removeClass("collapsed-box");
    $('#div2').show();
    $('#buton2 i').attr('class', 'fa fa-minus');

    $('#inputBusqueda').val("");
  }



}

