import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';
import { HttpService } from './../../services/http.service';


@Injectable()
export class BuscadorService {


  rutas: any;
  constructor(private ad:AngularFireDatabase, private _http:HttpService){
    this.getdataRuta();
  }

  getdataRuta(){
      this.ad.list('/rutas', {
        query: {
          orderByChild: 'ruta'
        }
      }).subscribe(data=>{
        this.rutas = data;
    });

  }

  getRuta(){
    return this.rutas;
  }

}
