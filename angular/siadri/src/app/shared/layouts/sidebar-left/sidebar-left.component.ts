import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar-left',
  templateUrl: './sidebar-left.component.html',
  styleUrls: ['./sidebar-left.component.css']
})

//CLASE CONTENEDORA DE LOS COMPONENTES AGGREMENTS (PARTE PUBLICA), BORRADORES, INSCRIPCIONES Y SOLICITUDES (SEGUNDO NIVEL)
export class SidebarLeftComponent implements OnInit {

  public admin = false;
  datosUser: any;
  rol:string;

  constructor() {
    if (localStorage.getItem('usuario')) {
       this.datosUser = JSON.parse(localStorage.getItem('usuario')); 
    }
  }

  ngOnInit() {
  }

}
